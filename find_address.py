#!/usr/bin/env python
import argparse
import pyperclip
import sys
import webbrowser

""" This script looks for an address on google maps. Address is entered
    as command line argument by the user. If no argument is given, address will
    be pasted from clipboard."""

base_url = 'https://google.com/maps/place/'

parser = argparse.ArgumentParser(description='Opens google maps in a webbrowser with specified street address.')
parser.add_argument('address', nargs='*', 
                    help='Street address. Using clipboard if address is not given.')

args = parser.parse_args()
if len(args.address) > 0:
    address = ' '.join(args.address)
else:
    address = pyperclip.paste()

print('Looking for address: {}'.format(address))
webbrowser.open(base_url + address)
